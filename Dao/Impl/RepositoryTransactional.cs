// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryTransactionalContacto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Examenneta.Dao.Impl { 
    using System;
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    ///  Clase que wrap el repository contacto para agregarle transactional
    /// </summary>
    public class RepositoryTransactionalContacto : IRepositoryContacto {

        /// <summary>
        /// Clase real de repository
        /// </summary>
        private readonly IRepositoryContacto repository;

        /// <summary>
        /// Transaction
        /// </summary>
        private readonly ITransactional transactional; 

        /// <summary>
        ///  Crea una instancia de RepositoryTransactionalContacto
        /// </summary>
        /// <param name="repository">Instancia de IRepositoryContacto</param>
        /// <param name="transactional">Transaciones</param>
        public RepositoryTransactionalContacto( IRepositoryContacto repository, ITransactional transactional){
            this.repository = repository;
            this.transactional = transactional;
        }

        /// <summary>
        /// Agrega un contacto
        /// </summary>
        /// <param name="contacto">Contacto a agregar</param>
        /// <returns>El id del nuevo contacto</returns>
        public async Task<Contacto> AddContacto(Contacto contacto){
            try{
                this.transactional.BeginTran();
                var result =  await this.repository.AddContacto(contacto);
                this.transactional.Commit();
                return result;
            }catch(Exception){
                this.transactional.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Libera los recursos
        /// </summary>
        public void Dispose(){
            this.repository.Dispose();
        }
    }

}