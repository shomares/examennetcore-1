// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryContacto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Examenneta.Dao.Impl { 
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    /// Interfaz que define a acceso a datos a Contactos
    /// </summary>
    public class RepositoryContacto : IRepositoryContacto {

        /// <summary>
        /// Acceso a Datos
        /// </summary>
        private readonly DataContext context; 

        /// <summary>
        /// Crrea una intancia de RepositoryContacto
        /// </summary>
        /// <param name="context">Acceso a datos</param>
        public RepositoryContacto(DataContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Agrega un contacto
        /// </summary>
        /// <param name="contacto">Contacto a agregar</param>
        /// <returns>El id del nuevo contacto</returns>
        public async Task<Contacto> AddContacto(Contacto contacto){
            this.context.Contactos.Add(contacto);
            await this.context.SaveChangesAsync();
            return contacto;

        }

        /// <summary>
        /// Libera los recursos
        /// </summary>
        public void Dispose(){
            this.context.Dispose();
        }

    }

}