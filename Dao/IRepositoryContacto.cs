// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepositoryContacto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Examenneta.Dao { 
    using System;
    using System.Threading.Tasks;
    using Models;
    
    /// <summary>
    /// Interfaz que define a acceso a datos a Contactos
    /// </summary>
    public interface IRepositoryContacto: IDisposable {

        /// <summary>
        /// Agrega un contacto
        /// </summary>
        /// <param name="contacto">Contacto a agregar</param>
        /// <returns>El id del nuevo contacto</returns>
        Task<Contacto> AddContacto(Contacto contacto);
    }

}