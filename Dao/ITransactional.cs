// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransactional.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Examenneta.Dao { 
    using System;
    using System.Threading.Tasks;
    using Models;
    
    /// <summary>
    /// Interfaz que define a acceso a datos a Contactos
    /// </summary>
    public interface ITransactional {

        /// <summary>
        /// Comienza la transaccion
        /// </summary>
        void BeginTran();

        /// <summary>
        /// Da commit a la transaccion
        /// </summary>
        void Commit();

        /// <summary>
        /// Regresa la transaccion
        /// </summary>
        void Rollback();
    }

}