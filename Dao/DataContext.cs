// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataContext.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Examenneta.Dao {
    using System.Threading.Tasks;
    using System;
    using Microsoft.EntityFrameworkCore.Storage;
    using Microsoft.EntityFrameworkCore;
    using Models;

    /// <summary>
    /// Clase de entity para acceso a datos
    /// </summary>
    public class DataContext : DbContext, ITransactional {

        /// <summary>
        /// Crea una instancia de acceso a Data Context
        /// </summary>
        /// <param name="options">Opciones de conexion</param>
        /// <returns>Una instancia de DatContext</returns>
        public DataContext (DbContextOptions<DataContext> options) : base (options) {

        }

        /// <summary>
        /// Representa a una tabla de base de datos
        /// </summary>
        /// <value>Tabla de base de datos</value>
        public DbSet<Contacto> Contactos { get; set; }

        /// <summary>
        /// Transaccion current
        /// </summary>
        private IDbContextTransaction transaction;

        /// <summary>
        /// Genera tablas
        /// </summary>
        /// <param name="modelBuilder">Crea la tabla</param>
        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            // Map table names
            modelBuilder.Entity<Contacto> ().ToTable ("Contacto");
            modelBuilder.Entity<Contacto> (entity => {
                entity.HasKey (e => e.IdPersona);
                entity.Property (e => e.NombreCompleto);
                entity.Property (e => e.Telefono);
                entity.Property (e => e.Email);
                entity.Property (e => e.FechaNacimiento);
            });

            base.OnModelCreating (modelBuilder);
        }

        /// <summary>
        /// Comienza la transaccion
        /// </summary>
        public void BeginTran () => this.transaction = this.Database.BeginTransaction ();

        /// <summary>
        /// Da commit a la transaccion
        /// </summary>
        public void Commit () {
            if (this.transaction != null) {
                this.transaction.Commit ();
            }

        }

        /// <summary>
        /// Regresa la transaccion
        /// </summary>
        public void Rollback () {
            if (this.transaction != null) {
                this.transaction.Rollback ();
            }

        }
    }

}