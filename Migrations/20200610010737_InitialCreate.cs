﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace examenneta.Migrations
{
    /// <summary>
    /// Clase de migracion para base de datos en sqllite
    /// </summary>
    public partial class InitialCreate : Migration
    {
        /// <summary>
        /// Crea las tablas
        /// </summary>
        /// <param name="migrationBuilder">Acceso a las tablas</param>
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contacto",
                columns: table => new
                {
                    IdPersona = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NombreCompleto = table.Column<string>(nullable: false),
                    Telefono = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    FechaNacimiento = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacto", x => x.IdPersona);
                });
        }

        /// <summary>
        /// Quitar tablas
        /// </summary>
        /// <param name="migrationBuilder">Acceso a bd</param>
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contacto");
        }
    }
}
