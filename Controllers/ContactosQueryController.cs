// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContactosQueryController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Examenneta.Controllers {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Microsoft.AspNet.OData;
    using Microsoft.AspNet.OData.Query;
    using Microsoft.AspNet.OData.Routing;
    using Dao;
    using Models;

    /// <summary>
    /// Contactos query controller
    /// </summary>
    [ODataRoutePrefix("Contacto")]
    public class ContactosQueryController : ODataController {

        /// <summary>
        /// Base de datos de contactos
        /// </summary>
        private readonly DataContext context;

        /// <summary>
        /// Crea una instancia de QueryController para OData
        /// </summary>
        /// <param name="context">Source de datos</param>
        public ContactosQueryController( DataContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Consulta todos los contactos para OData
        /// </summary>
        /// <returns>Tabla de contactos</returns>
        [EnableQuery]
        [ODataRoute]
        public IActionResult Get()
        {
            return this.Ok(this.context.Contactos);
        }

        /// <summary>
        /// Regresa un contacto
        /// </summary>
        /// <param name="id">Id a buscar</param>
        /// <returns>Un contacto</returns>
        [EnableQuery]
        [ODataRoute("({id})")]
        public IActionResult Get(int id)
        {
            var query = this.context.Contactos.Where( s=> s.IdPersona == id);

            if (query != null)
            {
                return this.Ok(query);
            }
            else{
                return this.NotFound();
            }
        }

    }
}