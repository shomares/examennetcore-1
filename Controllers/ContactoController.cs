﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContactoController.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Examenneta.Controllers {
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System;
    using Dao;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Models;

    /// <summary>
    /// Contactos controller
    /// </summary>
    [ApiController]
    [Route ("api/[controller]")]
    public class ContactoController : ControllerBase {

        /// <summary>
        /// Repository de contactos
        /// </summary>
        private readonly IRepositoryContacto repository;

        /// <summary>
        /// Crea una intancia del controlador
        /// </summary>
        /// <param name="repository">Repositorio de acceso a datos</param>
        public ContactoController (IRepositoryContacto repository) {
            this.repository = repository;
        }

        /// <summary>
        /// Agrega un contacto
        /// </summary>
        /// <param name="value">Contacto a guardar</param> 
        /// <response code="200">Regresa el producto con id</response>
        [HttpPost]
        public async Task<IActionResult> Post ([FromBody] Contacto value) {
            if (ModelState.IsValid) {
                var response = await this.repository.AddContacto (value);
                return this.Ok (response);
            } else {
                return this.NotFound();
            }

        }
    }
}