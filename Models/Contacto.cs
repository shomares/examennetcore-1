// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Contacto.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Examenneta.Models { 
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    
    /// <summary>
    /// Clase que reprensenta los contactos en base de datos
    /// </summary>
    public class Contacto{

        /// <summary>
        /// Email regex
        /// </summary>
        public const string EMAIL_REGEX = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        

        /// <summary>
        /// Valor Llave para contacto
        /// </summary>
        /// <value>Id de persona</value>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPersona 
        {   
            get;
            set;
        }
        
        /// <summary>
        /// Nombre completo del contacto
        /// </summary>
        /// <value>Nombre de la persona</value>
        [Required]
        public string NombreCompleto{
            get;
            set;
        }

        /// <summary>
        /// Telefono de la persona
        /// </summary>
        /// <value>Telefono</value>
        [Required]
        public string Telefono {
            get; 
            set;
        }

        /// <summary>
        /// Email de contacto de la persona
        /// </summary>
        /// <value>Email</value>
        [Required]
        [RegularExpression(EMAIL_REGEX)]
        public string Email{
            get; 
            set;
        }

        /// <summary>
        /// Fecha de nacimiento
        /// </summary>
        /// <value>Fecha</value>
        public DateTime?  FechaNacimiento {
            get;
            set;
        }
    }
}