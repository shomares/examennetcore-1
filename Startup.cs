// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Examenneta {
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System;
    using Dao.Impl;
    using Dao;
    using Microsoft.AspNet.OData.Builder;
    using Microsoft.AspNet.OData.Extensions;
    using Microsoft.AspNet.OData.Formatter;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpsPolicy;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Net.Http.Headers;
    using Microsoft.OData.Edm;
    using Microsoft.OpenApi.Models;
    using Models;
    using Swashbuckle.AspNetCore.Swagger;

    /// <summary>
    /// Configura la inyeccion de dependencias y la configuracion de proyecto
    /// </summary>
    public class Startup {

        /// <summary>
        /// Initaliza la clase de configuraion
        /// </summary>
        /// <param name="configuration">Congifuracion de la aplicacion</param>
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuracion de la aplicacion
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Obtiene el modelo para OData
        /// </summary>
        /// <returns>Un modelo para odata</returns>
        private IEdmModel GetEdmModel () {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder ();
            builder.EntitySet<Contacto> ("Contacto");
            return builder.GetEdmModel ();
        }

        /// <summary>
        /// Configura la inyeccion de Depedencias
        /// </summary>
        /// <param name="services">Kernel de inyecion</param>
        public void ConfigureServices (IServiceCollection services) {

            services.AddDbContext<DataContext> (optionsAction =>
                optionsAction.UseSqlite ("Filename=TestDatabase.db", options => {
                    options.MigrationsAssembly (Assembly.GetExecutingAssembly ().FullName);
                })
            );

            services.AddScoped<ITransactional> (s => s.GetService<DataContext> ());

            services.AddScoped<IRepositoryContacto> ((injected) => new RepositoryTransactionalContacto (
                new RepositoryContacto (injected.GetService<DataContext> ()),
                injected.GetService<ITransactional> ()));

            services.AddOData ();
            services.AddControllers ();
            services.AddMvc (options => {
                options.EnableEndpointRouting = false;

            });

            this.ConfigSwagger (services);
        }

        /// <summary>
        /// Configuracion de Swagger
        /// </summary>
        /// <param name="services"></param>
        private void ConfigSwagger (IServiceCollection services) {
            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", new OpenApiInfo { Title = "", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine (AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments (xmlPath);
            });

            services.AddMvcCore (options => {
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter> ().Where (_ => _.SupportedMediaTypes.Count == 0)) {
                    outputFormatter.SupportedMediaTypes.Add (new MediaTypeHeaderValue ("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter> ().Where (_ => _.SupportedMediaTypes.Count == 0)) {
                    inputFormatter.SupportedMediaTypes.Add (new MediaTypeHeaderValue ("application/prs.odatatestxx-odata"));
                }
            });

        }

        /// <summary>
        /// Configura el behavior de los http endpoints
        /// </summary>
        /// <param name="app">Configuracion de la aplicacion</param>
        /// <param name="env">Enviroment</param>
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            }

            app.UseHttpsRedirection ();

            app.UseMvc (p => {
                p.EnableDependencyInjection ();
                p.Select ().Expand ().Count ().Filter ().OrderBy ().MaxTop (100).SkipToken ().Build ();
                p.MapODataServiceRoute ("api", "api", this.GetEdmModel ());

                p.MapRoute (
                    name: "default",
                    template: "{controller}");

            });

            app.UseSwagger ();
            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Contactos V1");
            });

        }
    }
}