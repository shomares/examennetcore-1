// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Eneas Mejia">
//   shomares@gmail.com
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Examenneta {
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Inicia la aplicacion
    /// </summary>
    public class Program {
        
        /// <summary>
        /// Punto de acceso a la aplicacion
        /// </summary>
        /// <param name="args">Parametros externos</param>
        public static void Main (string[] args) {
            CreateHostBuilder (args).Build ().Run ();
        }

        /// <summary>
        /// Crea una instancia de Krestel
        /// </summary>
        /// <returns>Una instancia de krestel</returns>
        public static IHostBuilder CreateHostBuilder (string[] args) =>
            Host.CreateDefaultBuilder (args)
            .ConfigureWebHostDefaults (webBuilder => {
                webBuilder.UseStartup<Startup> ();
            });
    }
}